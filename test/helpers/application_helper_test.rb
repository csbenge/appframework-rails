require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "AppFramework for Rails"
    assert_equal full_title("Help"), "Help | AppFramework for Rails"
  end
end