# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.delete_all

User.create(:id => 1,
            :email =>'csbenge@gmail.com',
            :firstname => 'Carey',
            :lastname => 'Benge',
            :admin		=> true,
            :password => 'password',
            :password_confirmation => 'password',
            :activated => true,
            :activated_at => Time.zone.now)

User.create(:id => 2,
            :email =>'csbenge@yahoo.com',
            :firstname => 'Carey',
            :lastname => 'Benge',
            :admin		=> false,
            :password => 'password',
            :password_confirmation => 'password',
            :activated => true,
            :activated_at => Time.zone.now)

99.times do |n|
  firstname  = Faker::Name.name
  lastname  = Faker::Name.name
  email = "example-#{n+1}@afw.org"
  password = "password"
  User.create!(firstname:  firstname, lastname:  lastname,
               email: email,
               password:              password,
               password_confirmation: password)
end
