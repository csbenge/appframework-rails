module UsersHelper
	# Returns the Gravatar for the given user.
  def gravatar_for(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.firstname, class: "gravatar")
  end

  def prettyUpAdminRole(admin)
    
    if admin
      "<span class='label alert round'>#{admin}</span>"
    else 
    	"<span class='label info round'>#{admin}</span>"
    end
  end

end
