# Application Framework for Ruby on Rails Applications

This is the application framework for the
[*Ruby on Rails Applications*]
(http://www.careybenge.org/)
by [Carey Benge](http://www.careybenge.com/).